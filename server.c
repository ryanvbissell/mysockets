// vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab :
/*
 * Copyright (c) 2016, Ryan V. Bissell
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See the enclosed "LICENSE" file for exact license terms.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <string.h> /* memset */
#include <arpa/inet.h>  /* inet_ntoa */
#include <netdb.h>
#include <errno.h>

void perror_and_die(char const* message)
{
   perror(message);
   exit(EXIT_FAILURE);
}

int WaitForConnection(uint16_t port)
{
  int server_fd;
  int remote_fd;
  struct sockaddr_in server;
  struct sockaddr_in remote;
  struct sockaddr* pserver = (struct sockaddr*)&server;
  struct sockaddr* premote = (struct sockaddr*)&remote;

  if (0 > (server_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)))
    perror_and_die("socket() failed");

  if ( 0 > setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) )
    perror_and_die("setsockopt(SO_REUSEADDR) failed");

  // NOTE: 'SO_REUSEPORT' was first defined in Linux 3.9
  if ( 0 > setsockopt(server_fd, SOL_SOCKET, SO_REUSEPORT, &(int){1}, sizeof(int)) )
    perror_and_die("setsockopt(SO_REUSEPORT) failed");

  memset(&server, 0, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = htonl(INADDR_ANY);

  if ( 0 > bind(server_fd, pserver, sizeof(server)) )
    perror_and_die("bind() failed");

  if ( 0 > listen(server_fd, 1) )
    perror_and_die("listen() failed");

  getsockname(server_fd, pserver, &(int){sizeof(server)});
  port = ntohs(server.sin_port);

  fprintf(stderr, "Listening on port %d ...\n", port);
  remote_fd = accept(server_fd, premote, &(int){sizeof(remote)});
  if (0 > remote_fd)
    perror_and_die("accept() failed");

  if ( 0 > setsockopt(remote_fd, SOL_SOCKET, SO_KEEPALIVE, &(int){1}, sizeof(int)) )
    perror("setsockopt(SO_KEEPALIVE) failed");

  if ( 0 > setsockopt(remote_fd, IPPROTO_TCP, TCP_NODELAY, &(int){1}, sizeof(int)) )
    perror("setsockopt(TCP_NODELAY) failed");

  close(server_fd);

  fprintf(stderr, "...Connection established with %s\n",
                    inet_ntoa(remote.sin_addr) );

  return remote_fd;
}


void EchoClientMessages(int fd)
{
  int bytes;
  char buf[4096];
  size_t len = 1024;

  while ( len=1024, bytes = recv(fd, buf, len, 0) )
  {
    if (bytes < 0)
    {
      perror("recv() failed");
      return;
    }

    buf[bytes] = '\0';
    printf("RECEIVED: %s", buf);
  }

  if (!bytes)
    fprintf(stderr, "The client has closed the connection.\n");
}


void UsageAndExit(char const* program)
{
  fprintf(stderr, "USAGE: %s [port]\n", program);
  exit(EXIT_FAILURE);
}


int main(int argc, char** argv)
{
  uint16_t port=0;
  int remote_fd;

  if (argc > 2)
    UsageAndExit(argv[0]);

  if (argc == 2)
  {
    unsigned long lport;
    errno = 0;
    lport = strtoul(argv[1], NULL, 10);
    if (errno)
    {
      fprintf(stderr, "Invalid format for 'port' argument.\n");
      exit(EXIT_FAILURE);
    }
    port = (int)lport;
  }

  remote_fd = WaitForConnection(port);

  EchoClientMessages(remote_fd);
  close(remote_fd);

  exit(EXIT_SUCCESS);
}

