// vim: set tabstop=2 softtabstop=2 shiftwidth=2 expandtab :
/*
 * Copyright (c) 2016, Ryan V. Bissell
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See the enclosed "LICENSE" file for exact license terms.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <string.h> /* memset */
#include <stdbool.h>

void die(char const* message)
{
  fprintf(stderr, "%s\n", message);
  exit(EXIT_FAILURE);
}


void perror_and_die(int fd, char const* message)
{
   perror(message);
   if (fd != -1) close(fd);
   exit(EXIT_FAILURE);
}

void gaierror_and_die(int gai, char const* message)
{
  fprintf(stderr, "%s: %s\n", message, gai_strerror(gai));
  exit(EXIT_FAILURE);
}

static bool is_ipv6(char const* str)
{
  int count=0;

  // if we encounter more than one colon, assume IPv6
  while (str && *str && (count < 2))
  {
    if (*str++ == ':')
      ++count;
  }

  return (count > 1);
}


void ResolveNetworkName(char const* addrstr, char const* portstr,
                        uint32_t* addr,      uint16_t* port)
{
  struct addrinfo hints, *res;
  struct sockaddr_in *r;
  int err;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  if ( (err = getaddrinfo(addrstr, portstr, &hints , &res)) != 0)
    gaierror_and_die(err, addrstr);

  r = (struct sockaddr_in *) res->ai_addr;
  *addr = ntohl(r->sin_addr.s_addr);
  *port = ntohs(r->sin_port);

  freeaddrinfo(res);
}


void GetAddressAndPort(char const* arg, uint32_t* addr, uint16_t* port)
{
  char const* addrstr;
  char* portstr;

  if (is_ipv6(arg))
    die("IPv6 not supported.");

  if (!arg || !(portstr = strchr(arg, ':')))
    die("ERROR: ':<port>' argument required.");

  addrstr = NULL;
  *addr = htonl(INADDR_ANY);
  if (portstr != arg)
  {
    *portstr = '\0';
    addrstr = arg;
  }

  ++portstr;

  ResolveNetworkName(addrstr, portstr, addr, port);
}


int EstablishConnection(uint32_t addr, uint16_t port)
{
  int server_fd;
  struct sockaddr_in server;
  char addrstr[INET_ADDRSTRLEN];

  if (0 > (server_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)))
    perror_and_die(-1, "socket() failed");

  if ( 0 > setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) )
    perror_and_die(server_fd, "setsockopt(SO_REUSEADDR) failed");

  memset(&server, 0, sizeof(server));
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = htonl(addr);

  if (!inet_ntop(AF_INET, &(server.sin_addr), addrstr, INET_ADDRSTRLEN))
    perror_and_die(server_fd, "inet_ntop() failed");

  fprintf(stderr, "Attempting to connect to host '%s' on port %d ...\n",
                  addrstr, port);

  if (0 > connect(server_fd, (struct sockaddr*)&server, sizeof(server)) )
    perror_and_die(server_fd, "connect() failed");

  fprintf(stderr, "... Connection established!\n");
  return server_fd;
}


void SendServerMessages(int fd)
{
  char buf[1024];
  bool eof=false;

  fprintf(stderr, "Type messages that you want the server to echo.\n");
  fprintf(stderr, "End with Ctrl-D.\n\n");
  while (!eof)
  {
    fprintf(stdout, "SEND: ");
    eof = !fgets(buf, sizeof(buf), stdin);
    if (!eof)
    {
      size_t len = strlen(buf);
      size_t sent = send(fd, buf, len, 0);
      if (0 > sent)
        perror_and_die(fd, "send() failed");
    }
  }

  fprintf(stdout, "\n");
}


int main(int argc, char** argv)
{
  uint32_t addr;
  uint16_t port;
  int server_fd;

  GetAddressAndPort(argv[1], &addr, &port);

  server_fd = EstablishConnection(addr, port);

  SendServerMessages(server_fd);

  fprintf(stderr, "Closing the connection.\n");
  close(server_fd);

  exit(EXIT_SUCCESS);
}

